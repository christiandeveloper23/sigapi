<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatriculasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matriculas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('estudiante_id')->unsigned();
            $table->foreign("estudiante_id")->references('id')->on('estudiantes')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('estudiante__rerepresentante_id')->unsigned();
            $table->foreign("estudiante__rerepresentante_id")->references('id')->on('estudiante__rerepresentantes')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('grado_id')->unsigned();
            $table->foreign("grado_id")->references('id')->on('grados')->onDelete('cascade')->onUpdate('cascade');
            $table->string("tipo");
            $table->string("status");
            
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matriculas');
    }
}

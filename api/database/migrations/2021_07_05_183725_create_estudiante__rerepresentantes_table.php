<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstudianteRerepresentantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudiante__rerepresentantes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('estudiante_id')->unsigned();
            $table->foreign("estudiante_id")->references('id')->on('estudiantes')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('representante_id')->unsigned();
            $table->foreign("representante_id")->references('id')->on('representantes')->onDelete('cascade')->onUpdate('cascade');
            $table->string("Parentesco");


            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudiante__rerepresentantes');
    }
}

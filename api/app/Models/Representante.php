<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Representante extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function ERepresentantes(){

        return $this->belongsToMany(Estudiante_Rerepresentante::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    use HasFactory;

    public $timestamps = false;


    protected $fillable =['Nombres','Apellidos','fecha_nacimiento',

    'direccion',
    'genero'    
            ];


    public function ERepresentantes(){

        return $this->belongsToMany(Estudiante_Rerepresentante::class);
    }


    public function Matriculas() {
        return $this->hasMany(Matriculas::class);
    }
}

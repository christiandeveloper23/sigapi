<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estudiante_Rerepresentante extends Model
{
    use HasFactory;


    public $timestamps = false;

    public function Estudiantes(){

        return $this->belongsToMany(Estudiante::class);
    }


    public function Representantes(){

        return $this->belongsToMany(Representante::class);
    }


    public function Matriculas() {
        return $this->hasMany(Matriculas::class);
    }
}

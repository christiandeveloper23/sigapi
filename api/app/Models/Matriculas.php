<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Matriculas extends Model
{
    use HasFactory;
    public $timestamps = false;


    // protected $fillable =['id','estudiante_id','estudiante_representante_id',

    // 'grado_id',
    // 'tipo' ,
    // 'status',   
    //         ];



    public function Estudiante() {
        return $this->belongsTo(Estudiante::class);
    }


    public function Grado() {
        return $this->belongsTo(Grado::class);
    }


    public function EstudianteRepsentante() {
        return $this->belongsTo(Estudiante_Rerepresentante::class);
    }
}

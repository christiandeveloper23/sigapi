<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grado extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function Matriculas() {
        return $this->hasMany(Matriculas::class);
    }
}

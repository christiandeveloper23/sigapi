<?php

namespace App\Http\Controllers;

use App\Models\Estudiante;
use Illuminate\Http\Request;

class EstudianteController extends Controller
{
    public function getEstudiante(){

        return response()->json(Estudiante::all(),200);

    }


    public function getEstudianteid($id){

        $Estudiante = Estudiante::find($id);

        if(is_null($Estudiante)){

            return response()->json(["Mensaje"=>"Registro no encontrado"],404);
        }

        return response()->json($Estudiante::find($id),200);

    }


    public function store(Request $request)
    {
        $Estudiante = Estudiante::create($request->all());
        
        return response($Estudiante,201); 
    }


    public function update(Request $request, $id)
    {

        $Estudiante = Estudiante::find($id);
        if(is_null($Estudiante)){

            return response()->json(["Mensaje"=>"Registro no encontrado"],404);
        }

        $Estudiante->update($request->all());

        return response($Estudiante,201);
    }

    public function destroy($id)
    {

        $Estudiante = Estudiante::find($id);
        if(is_null($Estudiante)){

            return response()->json(["Mensaje"=>"Registro no encontrado"],404);
        }

        $Estudiante->delete();
        return response()->json(['Mensaje'=>"Registro eliminado"],200);

        
    }
   
}

<?php

namespace App\Http\Controllers;

use App\Models\Estudiante;
use App\Models\Estudiante_Rerepresentante;
use App\Models\Grado;
use App\Models\Matriculas;
use App\Models\Representante;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MatriculasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {



        $p = DB::table('matriculas')
        ->join('estudiantes','estudiantes.id' , '=' , 'matriculas.estudiante_id')
        ->join('grados','grados.id' , '=' , 'matriculas.grado_id')
        ->join('estudiante__rerepresentantes','estudiante__rerepresentantes.id','=','matriculas.estudiante__rerepresentante_id')
        ->join('representantes','representantes.id','=','estudiante__rerepresentantes.representante_id')
        ->select('matriculas.id as id',
        'estudiantes.Nombres as Nombres',
        'estudiantes.Apellidos as Apellidos',
        'grados.Grado as Grado',
        'matriculas.tipo as tipo',
        'matriculas.status as status',
        'representantes.Nombres as Names',
        'representantes.Apellidos as Lastnames',
        'estudiante__rerepresentantes.Parentesco as parentesco',
        
        
        )
        ->orderBy('id', 'desc')

        ->get();

        // dd($request->all());
        

        return response()->json($p,200);

         

    }


    //post
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Estudiante = Estudiante::create($request->all());
        $idE = $Estudiante->id;
        $Representante = Representante::create($request->all());
        $idR = $Representante->id;

        

       $Eyr = new Estudiante_Rerepresentante();
       $Eyr->estudiante_id = $idE;
       $Eyr->representate_id = $idR;
       $Eyr->Parentesco = $request;
       $Eyr->save();


       $ieyr=$Eyr->id;
       $matriculas = new Matriculas();
       $matriculas->estudiante_id = $idE;
       $matriculas->estudiante__rerepresentante_id = $ieyr;
       $matriculas->grado_id = $request;
       $matriculas->tipo = $request;
       $matriculas->status = $request;
       $matriculas->save();

       return response()->json([
        'res'=>true,
        'mesage' => 'Registro Creado'

       ],200);





    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Matriculas  $matriculas
     * @return \Illuminate\Http\Response
     */
    public function show(Matriculas $matriculas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Matriculas  $matriculas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Matriculas $matriculas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Matriculas  $matriculas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Matriculas $matriculas)
    {
        //
    }
}

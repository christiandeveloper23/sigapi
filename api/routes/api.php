<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('Estudiante','App\Http\Controllers\EstudianteController@getEstudiante');


Route::get('salsa','App\Http\Controllers\MatriculasController@index');

Route::post('salsan','App\Http\Controllers\MatriculasController@store');

Route::get('Estudiante/{id}','App\Http\Controllers\EstudianteController@getEstudianteid');


Route::post('Nuevo','App\Http\Controllers\EstudianteController@store');

Route::put('update/{id}','App\Http\Controllers\EstudianteController@update');
Route::delete('delete/{id}','App\Http\Controllers\EstudianteController@destroy');